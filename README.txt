WKProbability
-------------

You can get this program from the bitbucket repository and compile and install it using MAVEN.
The documentation is currently under development.

WSD experiments
===============

An example call to the example learner class using maven is:

mvn exec:java -X -Dexec.mainClass="org.wkp.learning.Learn" -Dexec.workingdir="..."

It relies on an experiment.properties file placed in the working directory.

An example experiment.properties for UMLS experiments is shown below.

The entry "kb.class" specifies the class used to process the knowledge base. For
processing the UMLS, the class is org.wkp.kb.UMLS2TermMatrix. This processing
class has three attributes, "umls.terms" pointing to a compressed version of the
the MRCONSO.RRF file, "umls.relations" pointing to a compressed version of the
MRREL.RRF.gz file and "umls.cooccurrences" that points to a folder with MetaMap
annotated XML files ending with extension "metamap.gz". This annotated file is
using during the model refinement based on examples from corpora.

Additional entries are required in the configuration file.
"p_w_c" is the name of the file in which the background probability based on
the knowledge base will be stored when processing the knowledge base or read
before disambiguation.
"output" contains the file in which the final model will be stored.

"benchmark" is a file with the ambiguous words in the evaluation set with
a list of the candidate senses.
"examples" is a folder that contains examples of the ambiguous words in context
with annotation of the correct sense. The extension of the example files is arff. 
The benchmark file and the examples follow the formats provided in the MSH WSD
data set available from the URL below and can be used to prepare additional
examples:
http://wsd.nlm.nih.gov/collaboration.shtml

umls.terms=.../UMLS2012AB/MRCONSO.RRF.gz
umls.relations=.../UMLS2012AB/MRREL.RRF.gz
umls.cooccurrences=.../wkpropability/examples

kb.class=org.wkp.kb.UMLS2TermMatrix
p_w_c=.../wkpropability/umls.p_w_c.gz

benchmark=.../wkpropability/babelnet_benchmark/benchmark.txt
examples=.../wkpropability/babelnet_benchmark

output=.../wkpropability/umls.model.gz


This software has been used in the following publication: 

Antonio Jimeno Yepes and Rafael Berlanga. "Knowledge based word-concept model estimation
and refinement for biomedical text mining." Journal of biomedical informatics (2014).